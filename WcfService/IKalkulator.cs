﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
namespace WcfServiceContract
{

    [ServiceContract]
    public interface IKalkulator
    {
        [OperationContract]
        long Add(long n1, long n2);

        [OperationContract]
        long Sub(long n1, long n2);

        [OperationContract]
        long Mul(long n1, long n2);

        [OperationContract]
        [FaultContract(typeof(MathFault))]
        long Div(long n1, long n2);

        [OperationContract]
        [FaultContract(typeof(MathFault))]
        long Rem(long n1, long n2);

        [OperationContract]
        string TimeCount(int value);

    }


}
