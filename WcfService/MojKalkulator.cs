﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfServiceContract
{

    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class MojKalkulator : IKalkulator
    {
        int suma = 0;


        public long Add(long n1, long n2)
        {
            long result = n1 + n2;
            Console.WriteLine("Wywołano operację Add, otrzymano: {0}, {1}, zwracany {2}", n1, n2, result);
            return result;
        }

        public long Sub(long n1, long n2)
        {
            long result = n1 - n2;
            Console.WriteLine("Wywołano operację Sub, otrzymano: {0}, {1}, zwracany {2}", n1, n2, result);
            return result;
        }

        public long Mul(long n1, long n2)
        {
            long result = n1 * n2;
            Console.WriteLine("Wywołano operację Mul, otrzymano: {0}, {1}, zwracany {2}", n1, n2, result);
            return result;
        }

        public long Div(long n1, long n2)
        {
            try
            {
                long result = n1 / n2;
                Console.WriteLine("Wywołano operację Div, otrzymano: {0}, {1}, zwracany {2}", n1, n2, result);
                return result;
            }
            catch (Exception)
            {
                MathFault mf = new MathFault();
                mf.ProblemDescription = "Dzielenie przez 0 nie jest możliwe!";
                throw new FaultException<MathFault>(mf);
            }
        }

        public long Rem(long n1, long n2)
        {
            try
            {
                long result = n1 % n2;
                Console.WriteLine("Wywołano operację Rem, otrzymano: {0}, {1}, zwracany {2}", n1, n2, result);
                return result;
            }
            catch (Exception)
            {
                MathFault mf = new MathFault();
                mf.ProblemDescription = "Modulo 0 nie jest możliwe!";
                throw new FaultException<MathFault>(mf);
            }
        }

        public string TimeCount(int value)
        {
            string result;
            if (value == 242455)
            {
                result = "Kamil " + DateTime.Now.ToString() + ", liczba odpowiedzi \"Nieuprawniony Użytkownik\": " + suma;
            }
            else
            {
                suma += 1;
                result = "Nieuprawniony Użytkownik";
            }
            Console.WriteLine("Wywołano operację TimeCount, otrzymano: {0}, zwracany {1}", value, result);
            return result;
        }
    }

}
