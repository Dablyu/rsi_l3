﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WcfServiceContract
{
    [DataContract]
    public class MathFault
    {
        private string problemDescription;

        [DataMember]
        public string ProblemDescription
        {
            get { return problemDescription; }
            set { problemDescription = value; }
        }
    }
}
