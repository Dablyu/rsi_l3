﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;

namespace WcfServiceHost
{
    class Program
    {
        static void Main(string[] args)
        {
            ServiceHost mojHost = new ServiceHost(typeof(WcfServiceContract.MojKalkulator));
            try
            {

                ServiceEndpoint endpoint1 = mojHost.Description.Endpoints.Find(typeof(WcfServiceContract.IKalkulator));
                ServiceEndpoint endpoint2 = mojHost.Description.Endpoints.Find(new Uri("http://localhost:10001/mojSerwis/endpoint2"));

                //wyswietl endpointy 
                Console.WriteLine("\n---> Endpointy:");


                Console.WriteLine("\nService endpoint {0}:", endpoint1.Name);
                Console.WriteLine("Binding: {0}", endpoint1.Binding.ToString());
                Console.WriteLine("ListenUri: {0}", endpoint1.ListenUri.ToString());

                Console.WriteLine("\nService endpoint {0}:", endpoint2.Name);
                Console.WriteLine("Binding: {0}", endpoint2.Binding.ToString());
                Console.WriteLine("ListenUri: {0}", endpoint2.ListenUri.ToString());




                mojHost.Open();
                Console.WriteLine("\n--> Serwis jest uruchomiony.");

                ContractDescription cd = ContractDescription.GetContract(typeof(WcfServiceContract.IKalkulator));
                Console.WriteLine("Informacje o kontrakcie:");
                Type contractType = cd.ContractType;
                Console.WriteLine("\tContract type: {0}", contractType.ToString());
                string name = cd.Name;
                Console.WriteLine("\tName: {0}", name);
                OperationDescriptionCollection odc = cd.Operations;
                Console.WriteLine("\tOperacje:");
                foreach (OperationDescription od in odc)
                {
                    Console.WriteLine("\t\t" + od.Name);
                }


                Console.WriteLine("\n--> Nacisnij <ENTER> aby zakonczyc.");
                Console.WriteLine();
                Console.ReadLine();
                mojHost.Close();
            }
            catch (CommunicationException ce)
            {
                Console.WriteLine("Wystapil wyjatek: {0}", ce.Message);
                mojHost.Abort();
            }
        }
    }
}
