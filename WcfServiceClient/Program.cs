﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using WcfServiceClient.ServiceReference1;
using WcfServiceContract;

namespace WcfServiceClient
{
    class Program
    {

        static void Main(string[] args)
        {
            KalkulatorClient klient1 = new KalkulatorClient("WSHttpBinding_IKalkulator");
            KalkulatorClient klient2 = new KalkulatorClient("mojEndpoint2");

            Console.WriteLine("Wywolania WSHttpBinding (klient 1):");
            
            Console.WriteLine("Wywołanie operacji Add");
            long result = klient1.Add(15, 4);
            Console.WriteLine("Wynik: {0}", result);

            Console.WriteLine("Wywołanie operacji Sub");
            result = klient1.Sub(15, 4);
            Console.WriteLine("Wynik: {0}", result);

            Console.WriteLine("Wywołanie operacji Mul");
            result = klient1.Mul(15, 4);
            Console.WriteLine("Wynik: {0}", result);

            Console.WriteLine("Wywołanie operacji Div");
            result = klient1.Div(15, 4);
            Console.WriteLine("Wynik: {0}", result);

            Console.WriteLine("Wywołanie operacji Div z dzieleniem przez 0");
            try
            {
                result = klient1.Div(15, 0);
                Console.WriteLine("Wynik: {0}", result);
            }
            catch (FaultException<MathFault> e)
            {
                Console.WriteLine("wtf:(");
                Console.WriteLine("Blad dzielenia! " + e.Detail.ProblemDescription);
            }

            Console.WriteLine("Wywołanie operacji Rem");
            result = klient1.Rem(15, 4);
            Console.WriteLine("Wynik: {0}", result);

            Console.WriteLine("Wywołanie operacji Rem z dzieleniem przez 0");
            try
            {
                result = klient1.Rem(15, 0);
                Console.WriteLine("Wynik: {0}", result);
            }
            catch (FaultException<MathFault> e)
            {
                Console.WriteLine("Blad dzielenia! " + e.Detail.ProblemDescription);
            }

            Console.WriteLine("Wywołanie operacji TimeCount z nieprawidlowym numerem");
            String textResult = klient1.TimeCount(32443);
            Console.WriteLine("Wynik: {0}", textResult);

            Console.WriteLine("Wywołanie operacji TimeCount z nieprawidlowym numerem");
            textResult = klient1.TimeCount(1);
            Console.WriteLine("Wynik: {0}", textResult);

            Console.WriteLine("Wywołanie operacji TimeCount z prawidlowym numerem");
            textResult = klient1.TimeCount(242455);
            Console.WriteLine("Wynik: {0}", textResult);

            Console.WriteLine();


            Console.WriteLine("Wywolania mojEndpoint2 (klient 2):");

            Console.WriteLine("Wywołanie operacji Add");
            result = klient2.Add(15, 4);
            Console.WriteLine("Wynik: {0}", result);

            Console.WriteLine("Wywołanie operacji Sub");
            result = klient2.Sub(15, 4);
            Console.WriteLine("Wynik: {0}", result);

            Console.WriteLine("Wywołanie operacji Mul");
            result = klient2.Mul(15, 4);
            Console.WriteLine("Wynik: {0}", result);

            Console.WriteLine("Wywołanie operacji Div");
            result = klient2.Div(15, 4);
            Console.WriteLine("Wynik: {0}", result);

            Console.WriteLine("Wywołanie operacji Div z dzieleniem przez 0");
            try
            {
                result = klient2.Div(15, 0);
                Console.WriteLine("Wynik: {0}", result);
            }
            catch (FaultException<MathFault> e)
            {
                Console.WriteLine("Blad dzielenia! " + e.Detail.ProblemDescription);
            }

            Console.WriteLine("Wywołanie operacji Rem");
            result = klient2.Rem(15, 4);
            Console.WriteLine("Wynik: {0}", result);

            Console.WriteLine("Wywołanie operacji Rem z dzieleniem przez 0");
            try
            {
                result = klient2.Rem(15, 0);
                Console.WriteLine("Wynik: {0}", result);
            }
            catch (FaultException<MathFault> e)
            {
                Console.WriteLine("Blad dzielenia! " + e.Detail.ProblemDescription);
            }

            Console.WriteLine("Wywołanie operacji TimeCount z nieprawidlowym numerem");
            textResult = klient2.TimeCount(32443);
            Console.WriteLine("Wynik: {0}", textResult);

            Console.WriteLine("Wywołanie operacji TimeCount z nieprawidlowym numerem");
            textResult = klient2.TimeCount(1);
            Console.WriteLine("Wynik: {0}", textResult);

            Console.WriteLine("Wywołanie operacji TimeCount z prawidlowym numerem");
            textResult = klient2.TimeCount(242455);
            Console.WriteLine("Wynik: {0}", textResult);

            Console.WriteLine();


            klient1.Close();
            klient2.Close();

        }
    }
}
